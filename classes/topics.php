<?php

/**
 * 
 *  @module      	Topics
 *  @author         Chio Maisriml, Dietrich Roland Pehlke, erpe
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

class topics extends LEPTON_abstract
{
    static $instance = NULL;
    
    const MODULURL = LEPTON_URL."/modules/topics/";
    const IMAGEDIR = LEPTON_URL."/modules/topics/img/";
    
    public function initialize()
    {
    
    }
}