<?php

/**
 * 
 *  @module      	Topics
 *  @author         Chio Maisriml, Dietrich Roland Pehlke, erpe
 *  @license        http://creativecommons.org/licenses/by/3.0/
 *  @platform       see info.php of this addon
 *  @license terms  see info.php of this addon
 *  @version        see info.php of this module
 *  
 *
 */

$files_to_register = array(
    'newuploader.php',
    'newuploader2.php'
);

LEPTON_secure::getInstance()->accessFiles( $files_to_register );

